Nuxeo installatsioon:
1.	Kopeerida kaust „demo_tenant“ kaustast "tenants" serverisse.
2.	Navigeerida terminalis kausta kus asub docker-compose.yml fail (demo_tenant kausta)
3.	Sisestada terminali käsk: „docker-compose up“ (kui tahate terminali taustal jooksutada neid protsesse, siis „docker-compose up -d“. Protsesside sulgemiseks „docker-compose stop/down“)
4.	Docker laeb alla ja installeerib kõik vajalikud ressursid avalikust internetist (selle vältimiseks tuleks hiljem siis tekitada endale kohalik repo nõutavatest docker image-itest). Käivitatakse neli komponenti eraldi Dockeri konteinerites:
	a.	Nuxeo veebirakendus
	b.	Redis teenus
	c.	Elasticsearch
	d.	Postgres andmebaas
5.	Kontrollida, kas teenus töötab aadressil: http://localhost:8080/nuxeo


Eelseadistatud töövoo paigaldamine Nuxeosse:
1.	Avada JSF ui aadress: http://localhost:8080/nuxeo/nxhome/default/@view_home
Kasutaja: Administrator
Parool: Administrator
2.	Navigeerida JSF UI-s: Admin tab -> Update Center -> local packages
3.	Valida „Upload package“
4.	Kopeerida Studios loodud pakett (project1-avalanche-mark-1.1.2.zip) serverile(jätkates eelmist punkti).
5.	Installeerida pakett.
6.	Taaskäivitada server adminiliidesest.
7.	Sellega luuakse kõik vajalikud dokumenditüübid, seadistused ja kasutajad.
		Kasutajate nimekiri:(Kõikidel on parool „123“ v-a Administrator)
			Document_admin
			Division_head
			Manager
			Representative
8.	Luua Nuxeo platvormil „reqAns“ (tähtis!) nimega mall failist „template.docx“ asukohta default-domain/templates (ligi pääseb jsf ui-s  WORKSPACES tab-ist) ja siduda see dokumenditüübiga „Citizen info request“/"request".
9.	Luua default-domain/workspaces alla kaust, kuhu saab luua teabenõudeid ehk Citizen Information Request-e (request failitüüp). Tavaline kaust sobib.
10.	Anda õigetele kasutajatele kaustale ligipääs (nt dokumendihaldurile õigused read/write) – muud kaustaga mitteseotud kasutajad näevad kausta sees olevaid teabenõude dokumente vaid siis, kui neil on selle konkreetse dokumendiga aktiivne tööülesanne.
11.	Kopeerida .jar lisamoodul (kaustast "bundles") serverile nxserver/bundles kausta käsureaga:
	„docker cp C:\Java\GetTime-core-1.0-SNAPSHOT.jar dimage_nuxeo_1:opt/nuxeo/server/nxserver/bundles“
12.	Taaskäivitada server
